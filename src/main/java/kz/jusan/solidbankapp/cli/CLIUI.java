package kz.jusan.solidbankapp.cli;

import kz.jusan.solidbankapp.account.AccountType;

public interface CLIUI extends CreateAccountOperationUI, WithdrawDepositOperationCLIUI{
    public double requestClientAmount();

    public Long requestClientAccountNumber();

    @Override
    public AccountType requestAccountType();
}
