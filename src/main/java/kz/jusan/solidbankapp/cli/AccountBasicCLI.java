package kz.jusan.solidbankapp.cli;

import kz.jusan.solidbankapp.BankCore;
import kz.jusan.solidbankapp.account.Account;
import kz.jusan.solidbankapp.account.AccountType;
import kz.jusan.solidbankapp.service.AccountListingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
@Component
@RequiredArgsConstructor
public class AccountBasicCLI {
    private final CreateAccountOperationUI createAccountOperationUI;
    private final BankCore bankCore;
    private final AccountListingService accountListing;

    public void createAccountRequest(String clientId) {
        AccountType accountType = createAccountOperationUI.requestAccountType();
        bankCore.createNewAccount(accountType, clientId);
    }
    public void getAccounts(String clientId) {

        Iterable<Account> accountList = this.accountListing.getClientAccounts(clientId);
        System.out.println("[");
        if(accountList != null) {
            for (Account account : accountList) {
                System.out.println(account);
            }
            System.out.println("]");
        }
    }
}
