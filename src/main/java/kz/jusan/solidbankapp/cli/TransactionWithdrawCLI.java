package kz.jusan.solidbankapp.cli;

import kz.jusan.solidbankapp.account.Account;
import kz.jusan.solidbankapp.service.AccountListingService;
import kz.jusan.solidbankapp.transaction.TransactionWithdraw;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TransactionWithdrawCLI {
    private TransactionWithdraw transactionWithdraw;
    private WithdrawDepositOperationCLIUI withdrawDepositOperationCLIUI;
    private AccountListingService accountListing;

    public void withdrawMoney(String clientId) {
        double amount = withdrawDepositOperationCLIUI.requestClientAmount();
        Long accountId = withdrawDepositOperationCLIUI.requestClientAccountNumber();
        Account accountWithdraw = null;

        if(accountId>0)
            accountWithdraw = accountListing.getClientWithdrawAccount(clientId, accountId);

        if(accountWithdraw.isWithdrawAllowed())
            transactionWithdraw.execute(amount, accountWithdraw);
        else
            System.out.println("withdraw NOT allowed on FIXED account");
    }
}
