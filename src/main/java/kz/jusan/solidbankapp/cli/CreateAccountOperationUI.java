package kz.jusan.solidbankapp.cli;

import kz.jusan.solidbankapp.account.AccountType;

public interface CreateAccountOperationUI {
    public AccountType requestAccountType();
}
