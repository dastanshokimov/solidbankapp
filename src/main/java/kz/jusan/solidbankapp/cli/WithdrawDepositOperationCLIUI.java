package kz.jusan.solidbankapp.cli;

public interface WithdrawDepositOperationCLIUI {
    public double requestClientAmount();

    public Long requestClientAccountNumber();
}
