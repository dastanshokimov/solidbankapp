package kz.jusan.solidbankapp.cli;

import kz.jusan.solidbankapp.account.Account;
import kz.jusan.solidbankapp.service.AccountListingService;
import kz.jusan.solidbankapp.transaction.TransactionDeposit;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TransactionDepositCLI {
    private TransactionDeposit transactionDeposit;
    private WithdrawDepositOperationCLIUI withdrawDepositOperationCLIUI;
    private AccountListingService accountListing;

    public void depositMoney(String clientId) {

        double amount = withdrawDepositOperationCLIUI.requestClientAmount();
        long accountId = withdrawDepositOperationCLIUI.requestClientAccountNumber();
        Account account = null;
        if(accountId> 0)
            account = accountListing.getClientAccount(clientId, accountId);
        if(account != null)
            transactionDeposit.execute(amount, account);
        else
            System.out.println("Account not found");
    }
}
