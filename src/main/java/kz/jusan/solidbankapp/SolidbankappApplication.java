package kz.jusan.solidbankapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolidbankappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolidbankappApplication.class, args);
	}

}
