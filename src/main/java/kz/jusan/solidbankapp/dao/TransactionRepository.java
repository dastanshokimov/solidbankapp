package kz.jusan.solidbankapp.dao;

import kz.jusan.solidbankapp.transaction.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {
    List<Transaction> findTransactionsByAccountId(Long accountId);
}
