package kz.jusan.solidbankapp.auth;

import kz.jusan.solidbankapp.exception.AlreadyHasThatUsernameException;
import kz.jusan.solidbankapp.response.UserCreatedSuccessfullyResponse;
import kz.jusan.solidbankapp.service.CustomUserCreationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/users")
@RestController
@AllArgsConstructor
public class CustomUserController {
    private CustomUserCreationService customUserCreationService;
    @GetMapping()
    public List<CustomUser> printAllUsers() {
        return customUserCreationService.getAllCustomUsers();
    }

    @PostMapping("/register")
    public ResponseEntity<Object> createCustomUser(@RequestBody CustomUser customUser) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(16);
        CustomUser foundUser = customUserCreationService.getCustomUserByUsername(customUser.getUsername());

        if(foundUser != null) {
            throw new AlreadyHasThatUsernameException(customUser.getUsername());
        }

        CustomUser newCustomUser = CustomUser.builder()
                                .username(customUser.getUsername())
                                .password(encoder.encode(customUser.getPassword()))
                .build();

        customUserCreationService.createUser(newCustomUser);
        return new ResponseEntity<>(new UserCreatedSuccessfullyResponse(
                "User created successfully", newCustomUser, HttpStatus.OK), HttpStatus.OK);
    }
}
