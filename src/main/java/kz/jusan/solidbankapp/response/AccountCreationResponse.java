package kz.jusan.solidbankapp.response;

import kz.jusan.solidbankapp.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class AccountCreationResponse {
    private String message;
    private Account account;
    private HttpStatus statusCode;
}
