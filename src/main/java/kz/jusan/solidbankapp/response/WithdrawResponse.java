package kz.jusan.solidbankapp.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class WithdrawResponse {
    private String message;
    private HttpStatus statusCode;
}
