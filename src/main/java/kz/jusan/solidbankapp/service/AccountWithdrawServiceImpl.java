package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.account.Account;
import kz.jusan.solidbankapp.dao.AccountRepository;
import kz.jusan.solidbankapp.exception.WithdrawException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class AccountWithdrawServiceImpl implements AccountWithdrawService {
    @Autowired
    private AccountRepository accountRepository;
    @Override
    public void withdraw(double amount, Account account) {
        double withdrawResult = account.getBalance()-amount;
        if(account.isWithdrawAllowed()) {
            if(withdrawResult >= 0) {
                String accountId  = String.format("%03d%06d", 1, Integer.parseInt(account.getId()+""));
                System.out.println(amount + "$ transferred from " + accountId + " account");
                account.setBalance(withdrawResult);
                accountRepository.save(account);
            } else {
                throw new WithdrawException("Withdraw not possible: not enough money");
            }
        } else {
            throw new WithdrawException("WITHDRAW NOT ALLOWED on FIXED ACCOUNT");
        }
    }
}
