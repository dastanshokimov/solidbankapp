package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.account.AccountType;


public interface AccountCreationService {

    public void create(AccountType accountType, long bankId, String clientId, long accountId);

}
