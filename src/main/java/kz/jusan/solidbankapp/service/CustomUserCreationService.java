package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.auth.CustomUser;

import java.util.List;

public interface CustomUserCreationService {
    public void createUser(CustomUser customUser);

    public CustomUser getCustomUserByUsername(String username);

    public List<CustomUser> getAllCustomUsers();
}
