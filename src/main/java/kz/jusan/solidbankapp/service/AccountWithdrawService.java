package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.account.Account;


public interface AccountWithdrawService {
    public void withdraw(double amount, Account account);
}
