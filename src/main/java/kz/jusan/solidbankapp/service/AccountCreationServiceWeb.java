package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.account.Account;

public interface AccountCreationServiceWeb {
    public Long createAccount(Account account);
}
