package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.account.Account;


public interface AccountDepositService {
    public void deposit(double amount, Account accountWithdraw);
}
