package kz.jusan.solidbankapp.service;

public interface AccountDeletionService {
    public void deleteAccountById(Long accountId);
}
