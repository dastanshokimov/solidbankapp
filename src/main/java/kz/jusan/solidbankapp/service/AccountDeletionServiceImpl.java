package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.dao.AccountRepository;
import kz.jusan.solidbankapp.exception.AccountNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountDeletionServiceImpl implements AccountDeletionService {
    private AccountRepository accountRepository;

    @Override
    public void deleteAccountById(Long accountId) {
        accountRepository.findById(accountId)
                        .orElseThrow(() -> new AccountNotFoundException(accountId));
        accountRepository.deleteById(accountId);
    }
}
