package kz.jusan.solidbankapp.service;

import kz.jusan.solidbankapp.auth.CustomUser;
import kz.jusan.solidbankapp.auth.CustomUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CustomUserCreationServiceImpl implements CustomUserCreationService {
    private CustomUserRepository customUserRepository;
    @Override
    public void createUser(CustomUser customUser) {
        customUserRepository.save(customUser);
    }

    public CustomUser getCustomUserByUsername(String username) {
        return customUserRepository.findCustomUserByUsername(username);
    }

    public List<CustomUser> getAllCustomUsers() {
        return customUserRepository.findAll();
    }
}
