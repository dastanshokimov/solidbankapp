# SOLIDBankApp

SOLIDBankApp for Advanced Java Backend topic on stepik.org 

## Contributors 

`Makhambet Torezhan`
`Zhandos Yernazarov`
`Dastan Shokimov`

## Getting started



## Initial Trello board 

You can see tasks for Sprints 1 & 2 based on the features required to be present in our app. Apart from that there is a general information about the Project Management and the link to our `GitLab` repository. 

![img.png](img.png)

## Final Trello board

![final-trello](https://i.imgur.com/XBZo0Ur.png)

